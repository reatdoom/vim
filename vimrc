"=============================================================================
" File:       vimrc
" Author:     reatdoom <reatdoom@gmail.com>
" Date:       2008/07/10
"=============================================================================

" Startup {{{
filetype indent plugin on

augroup vimrcEx
    au!
    autocmd FileType text setlocal textwidth=78
augroup END

" vim 文件折叠方式为 marker
augroup ft_vim
    au!
    au FileType vim setlocal foldmethod=marker
    autocmd BufReadPost *
      \ if line("'\"") > 1 && line("'\"") <= line("$") |
      \   exe "normal! g`\"" |
      \ endif
augroup END
" }}}

" Setting {{{
set nobackup
" set nocompatible
set history=1024
set autoindent
set cindent
set ignorecase
set cc=120
set clipboard+=unnamed
set completeopt=longest,menu
set laststatus=2
set pastetoggle=<F2>
" 去掉分隔线
set fillchars=vert:\ 

set background=dark
set cursorline
set hlsearch
set number
set relativenumber

" 设置Tab与行尾标识符
set listchars=tab:»\ ,eol:¬,trail:·,extends:>,precedes:<

set guioptions-=L
set guioptions-=r
" }}}

" Lang & Encoding {{{
set encoding=utf-8
set fileencodings=utf-8,gb2312,gbk,gb18030,cp936
set fileencoding=utf-8
" }}}

" Format {{{
set wrap
set autoindent
set smartindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
" 拆叠
set foldmethod=indent
set foldcolumn=0
set foldlevel=20
syntax on
" }}}

" Keymap {{{
let mapleader=";"

"inoremap <esc> <nop>
"inoremap jk <esc>

" vimrc
nmap <leader>s :source $MYVIMRC<cr>
nmap <leader>e :e $MYVIMRC<cr>

" Tab操作

nmap <leader>tn :tabnew<cr>
nmap <leader>tc :tabclose<cr>
nmap <leader>th :tabp<cr>
nmap <leader>tl :tabn<cr>

" 正常模式下 alt+j,k,h,l 调整分割窗口大小
nnoremap <M-j> :resize +5<cr>
nnoremap <M-k> :resize -5<cr>
nnoremap <M-h> :vertical resize -5<cr>
nnoremap <M-l> :vertical resize +5<cr>


" 窗口操作

nmap <C-j> <C-W>j
nmap <C-k> <C-W>k
nmap <C-h> <C-W>h
nmap <C-l> <C-W>l

" Buffer操作
nnoremap <C-left> :bp<CR>
nnoremap <C-right> :bn<CR>

" 移动光标

nnoremap j gj
nnoremap k gk

" 插入模式移动光标 alt + 方向键
inoremap <M-j> <Down>
inoremap <M-k> <Up>
inoremap <M-h> <left>
inoremap <M-l> <Right>

" 命令模式下的行首尾
cnoremap <C-a> <home>
cnoremap <C-e> <end>


" 折叠

nnoremap <space> za
nmap <leader>f0 :set foldlevel=0<CR>
nmap <leader>f1 :set foldlevel=1<CR>
nmap <leader>f2 :set foldlevel=2<CR>
nmap <leader>f3 :set foldlevel=3<CR>
nmap <leader>f4 :set foldlevel=4<CR>
nmap <leader>f5 :set foldlevel=5<CR>
nmap <leader>f6 :set foldlevel=6<CR>
nmap <leader>f7 :set foldlevel=7<CR>
nmap <leader>f8 :set foldlevel=8<CR>
nmap <leader>f9 :set foldlevel=9<CR>
nmap <leader>fa :set foldlevel=10<CR>
nmap <leader>fb :set foldlevel=11<CR>
nmap <leader>fc :set foldlevel=12<CR>
nmap <leader>fd :set foldlevel=13<CR>
nmap <leader>fe :set foldlevel=14<CR>
nmap <leader>ff :set foldlevel=15<CR>


" 其他

nmap <silent> <leader>/ :nohlsearch<CR>

" 打开当前目录 windows
nmap <silent> <leader>o :!open %:p:h<CR>

vnoremap <Leader>y "+y
nmap <leader>p "+p
" }}}

" Plugin {{{
filetype off

set nocompatible
set rtp+=~/.vim/bundle/vundle

call plug#begin('~/.vim/plugged')
" ----- Window: Winmanager ----- {{{
" Plug 'winmanager'
"
"let g:AutoOpenWinManager = 1
"let g:NERDTree_title="NERDTree"
"let g:winManagerWindowLayout='NERDTree'
"let g:winManagerWidth= 35
" }}}

" ----- Window: NerdTree ----- {{{
Plug 'scrooloose/nerdtree'

let NERDTreeBookmarksSort = 1
let NERDTreeIgnore=['.idea', '.vscode', '*.pyc']
let NERDTreeMinimalUI = 1
let NERDTreeShowBookmarks = 1
let NERDTreeShowLineNumbers = 0
let NERDTreeWinSize=35
let g:NERDTreeDirArrowCollapsible = '▾'
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeSortOrder = ['\/$', '\.html$', '\.css$', '\.js$', '\.php$', '\.py$']
let g:NERDTreeWinPos = 'left'

map <F3> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") &&b:NERDTreeType == "primary") | q | endif
autocmd vimenter * NERDTree
function! NERDTree_Start()
    exec 'NERDTree'
endfunction

function! NERDTree_IsValid()
    return 1
endfunction
" }}}

" ----- Window: Tlist ----- {{{
Plug 'vim-scripts/taglist.vim'

let Tlist_Auto_Highlight_Tag = 1
let Tlist_Auto_Update = 1
let Tlist_Exit_OnlyWindow = 1
let Tlist_File_Fold_Auto_Close = 1
let Tlist_Highlight_Tag_On_BufEnter = 1
let Tlist_Inc_Winwidth=0
let Tlist_Show_One_File=1
let Tlist_Use_Right_Window = 0
let Tlist_Use_SingleClick = 1
let Tlist_WindWidth=40
" }}}

" ----- Fugitive ----- {{{
" 显示git分支状态
Plug 'tpope/vim-fugitive'
" }}}

" ----- Markdown ----- {{{
Plug 'plasticboy/vim-markdown'

let g:vim_markdown_frontmatter=1
" }}}

" ----- Speedy: Tabular ----- {{{
Plug 'godlygeek/tabular'

nmap <Leader>a= :Tabularize /=<CR>
vmap <Leader>a= :Tabularize /=<CR>
nmap <Leader>a: :Tabularize /:\zs<CR>
vmap <Leader>a: :Tabularize /:\zs<CR>
" }}}

" ----- Speedy: Emmet ----- {{{
Plug 'mattn/emmet-vim'
" }}}

" ----- Speedy: Ultisnips ----- {{{
Plug 'SirVer/ultisnips'

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
"let g:UltiSnipsEditSplit="vertical"
" }}}

" ----- Ctrlp ----- {{{
Plug 'kien/ctrlp.vim'

let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:10,results:10'
set wildignore+=*\\.git\\*,*\\tmp\\*,*.swp,*.zip,*.exe,*.pyc
" }}}

" ----- Theme: Airline ----- {{{
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='tomorrow'
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" }}}

" ----- Theme: Solarized ----- {{{
Plug 'altercation/vim-colors-solarized'
let g:solarized_visibility = "high"
let g:solarized_contrast = "high"
let g:solarized_termcolors= 256
" }}}

" ----- Theme: base16-vim ----- {{{
Plug 'chriskempson/base16-vim'
" }}}

" ----- Theme: vim-devicons ----- {{{
"Plugin 'ryanoasis/vim-devicons'
" }}}

" ----- tern_for_vim ----- {{{
Plug 'ternjs/tern_for_vim'

let tern_show_signature_in_pum = 1
let tern_show_argument_hints = 'on_hold'
autocmd FileType javascript nnoremap <leader>d :TernDef<CR>
autocmd FileType javascript setlocal omnifunc=tern#Complete
" }}}

" ----- YouCompleteMe ----- {{{
" Plug 'Valloric/YouCompleteMe'
 
let g:ycm_min_num_of_chars_for_completion = 3
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_complete_in_comments = 1
let g:ycm_error_symbol = '✘'
let g:ycm_warning_symbol = '❗'
let g:ycm_use_ultisnips_completer = 1
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'
let g:ycm_semantic_triggers =  {
            \   'c' : ['->', '.'],
            \   'objc' : ['->', '.'],
            \   'ocaml' : ['.', '#'],
            \   'cpp,objcpp' : ['->', '.', '::'],
            \   'perl' : ['->'],
            \   'php' : ['->', '::', '(', 'use ', 'namespace ', '\'],
            \   'cs,java,typescript,d,python,perl6,scala,vb,elixir,go' : ['.', 're!(?=[a-zA-Z]{3,4})'],
            \   'html': ['<', '"', '</', ' '],
            \   'vim' : ['re![_a-za-z]+[_\w]*\.'],
            \   'ruby' : ['.', '::'],
            \   'lua' : ['.', ':'],
            \   'erlang' : [':'],
            \   'haskell' : ['.', 're!.'],
            \   'scss,css': [ 're!^\s{2,4}', 're!:\s+' ],
            \   'javascript': ['.', 're!(?=[a-zA-Z]{3,4})'],
            \ }

nnoremap <Leader>jd :YcmCompleter GoToDefinitionElseDeclaration<CR>
" }}}

" ----- syntastic ----- {{{
Plug 'scrooloose/syntastic'

let g:syntastic_error_symbol='✘'
let g:syntastic_warning_symbol='❗'
let g:syntastic_style_error_symbol='»'
let g:syntastic_style_warning_symbol='•'
let g:syntastic_check_on_open=1
let g:syntastic_enable_highlighting = 0
let g:syntastic_javascript_checkers = ['eslint']
" }}}

" ----- nerdcommenter ----- {{{
Plug 'scrooloose/nerdcommenter'
let g:NERDSpaceDelims=1
" }}}

" ----- LeaderF ----- {{{
Plug 'Yggdroot/LeaderF', { 'do': './install.sh' }
let g:Lf_ShortcutF = '<c-p>'
let g:Lf_ShortcutB = '<m-n>'
noremap <c-n> :LeaderfMru<cr>
noremap <m-p> :LeaderfFunction!<cr>
noremap <m-n> :LeaderfBuffer<cr>
noremap <m-m> :LeaderfTag<cr>
let g:Lf_StlSeparator = { 'left': '', 'right': '', 'font': '' }

let g:Lf_RootMarkers = ['.project', '.root', '.svn', '.git']
let g:Lf_WorkingDirectoryMode = 'Ac'
let g:Lf_WindowHeight = 0.30
let g:Lf_CacheDirectory = expand('~/.vim/cache')
let g:Lf_ShowRelativePath = 0
let g:Lf_HideHelp = 1
let g:Lf_StlColorscheme = 'powerline'
let g:Lf_PreviewResult = {'Function':0, 'BufTag':0}
" }}}

Plug 'vim-scripts/c.vim'
Plug 'honza/vim-snippets'
Plug 'elzr/vim-json'
Plug 'nvie/vim-flake8'
Plug 'rizzatti/dash.vim'
Plug 'burnettk/vim-angular'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-surround'
Plug 'ap/vim-css-color'
call plug#end()
filetype on
" }}}

" GUI {{{
" colorscheme Tomorrow-Night

if has('gui_running')
  set guifont=Source\ Code\ Pro\ for\ Powerline:h13
endif

if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif

" 空格标识
"highlight ForbiddenWhitespace ctermbg=010
"match ForbiddenWhitespace /\s\+$\|\t/
"autocmd InsertEnter * match ForbiddenWhitespace /\t\|\s\+\%#\@<!$/
" }}}

" vim:fen:fdm=marker:fmr={{{,}}}:fdl=0:fdc=1:ts=2:sw=2:sts=2
